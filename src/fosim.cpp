#include "fosim/fosim.h"


/**
 * @brief Namespace containing methods and variables in order to get an easier access to useful properties and methods of OpenSim software.
 * 
 */

namespace fosim{

  /**
   * @brief Default constructor of the OsimModel object. Nothing is initialized here.
   * 
   */
  OsimModel::OsimModel()
  {
    
  }

  /**
   * @brief Full constructor of the OsimModel object. Initialize the main properties of the object
   * (path to file, path to .vtp files).
   * 
   * @param model_path_ : Path to the .osim file.
   * @param geometry_path_ : Path to the .vtp files of the object.
   * @param setup_ik_file_ ; (Depreciated) Path to the .xml file used for IK. 
   */
  OsimModel::OsimModel(std::string model_path_, std::string geometry_path_, std::string setup_ik_file_)
  {
    model_path = model_path_;
    geometry_path = geometry_path_;
    setup_ik_file = setup_ik_file_;
    fromScratch();

  }

  /**
   * @brief Initialize the most basic variables of the OsimModel object. 
   * Here, these variables should not be modified as long as the body structure of
   * the model stays the same (same body parts and joints, same name and number of markers).
   * 
   */
  void OsimModel::fromScratch()
  {
    osimModel = osim::Model(model_path);
    osimState = osimModel.initSystem();
    osimModel.realizePosition(osimState);
    D_child_parent = getChildParentBody(osimModel);

    D_Q_coord = getCorresQCoord(osimModel,osimState);
    setToNeutral();
    D_coord_Q = getCorresCoordQ(osimModel,osimState);
    setToNeutral();
    D_alph_multi = getCorresAlphMulti(osimModel,osimState);
    D_ground_props = getGroundProperties(osimModel,osimState);

    D_body_markers = getMarkersByBodyParts(osimModel,osimState);
    D_markers_body = getBodyByMarkers(osimModel,osimState);

    getMarkersBodiesInMultibodyOrder(D_child_parent,D_body_markers, markers_bodies_multiorder, D_parent_all_children);


    totalUpdModel();

  }

  /**
   * @brief Update variables that may change after an update of the characteristics of the model 
   * (ex : change in marker positions in local bodies).
   * 
   */
  void OsimModel::totalUpdModel()
  {
    getDistanceBetweenMarkers(osimModel ,osimState, min_mark_dist, max_mark_dist);

    lowest_mark = getLowestOsimMarker(osimModel,osimState);

    D_jp = getJointsTransform(osimModel,osimState,"Parent",0);
    D_mark_pos_local = getMarkersPositions(osimModel,osimState,"Local");



    D_mark_dist = getClosestMarkerFromOtherBody(osimModel, osimState);
    D_mark_closest_dist = getClosestMarkerInBody(osimModel,osimState);
    D_mark_all_dist = getMarkerDistancesInBody(osimModel,osimState);

    D_markers_weight = computeMarkersWeights( markers_bodies_multiorder, D_child_parent, D_mark_pos_local, D_body_markers);

    updModel();

  }

  /**
   * @brief Given to an OsimModel given as an argument the properties of the OsimModel used to call this method.
   * Ex : osModA.clone3(osModB, args*) : the properties of osModA will be saved into osModB.
   * 
   * @param osModClone : The new clone of the OsimModel object.
   * @param setQ Boolean indicating whether joints values should also be cloned or not.
   * @param setMarkers Boolean indicating whether markers positions should also be cloned or not.
   */
  void OsimModel::clone3(OsimModel & osModClone, bool setQ, bool setMarkers)
  {
    osModClone.model_path = model_path;
    osModClone.geometry_path = geometry_path;
    osModClone.setup_ik_file = setup_ik_file;
    osModClone.fromScratch();

    // set Q vector
    if (setQ)
    {
      osModClone.setQ(D_joints_values);
    }

    if (setMarkers)
    {
      // set Markers Positions
      osModClone.setMarkersPositions(D_mark_pos);
    }

  }

/**
 * @brief Convenient method to call Inverse Kinematics.
 * First, the joint configuration of the model is computed thanks to Opensim method.
 * However, as Opensim IK algorithm do not include any constraint on the computed solution
 * for Inverse Kinematics, a check is added to the computed solution.
 * 
 * @param markers_positions 
 * @param markers_weights 
 * @param mode 
 * @param time 
 * @return int result : =-1 if failed; 0 if succeded with correction; 1 if succeded without issues
 */
int OsimModel::IK(std::map<std::string,Eigen::Vector3d> markers_positions, std::map<std::string,double> markers_weights, int mode, double time, std::map<std::string,double> coordinates_weights, bool verbose )
{
  if (coordinates_weights.size() != markers_positions.size())
  {
    for (std::map<std::string,double>::iterator it = D_joints_values.begin(); it!= D_joints_values.end(); it++)
    {
      coordinates_weights[it->first] = 1.0;
    }
  }
  int result = IKFromMarkersPositionsAndWeights(osimModel,osimState,markers_positions,markers_weights,coordinates_weights,mode,time);

  if (result == 1)
  {
    std::map<std::string,double> Djv = getJointsValues(osimModel,osimState);
    // filtering operation on joints values
    result = setQ(Djv, verbose);
    // updating of model
    updModel();
  }
  return(result);
}


/**
 * @brief Given markers associated with the theorical opensim markers and expressed into the ground referential (called markersLocation), 
 * and scaling factors (called stretching_vector) for each of the body parts of the model, apply those scaling factors to each of the 
 * opensim markers.
 * To do so, express the markers into the given referential, apply the corresponding stretching factors, then reexpress 
 * the markers into the ground referential. 
 * We suppose that the model is already at a configuration that is matching the position of the markers.
 * 
 * @param markersLocation Position of the Opensim markers to which the stretching operation should be applied. The referential in which those markers
 * are expressed should be set by the referential variable.
 * @param stretching_vector Stretching factors to apply in each axis of the local bodies (X,Y,Z). Organized by body_name:stretching_factor.
 * @param referential Name of the referential on which stretching parameters should be applied to all the given markers.
 * Default value is "local". This means that for each marker, the stretching will be applied into their respective local body
 * referential.
 */
void OsimModel::applyStretching(std::map<std::string,Eigen::Vector3d> & markersLocation, std::map<std::string,std::vector<double> > stretching_vector,
std::string referential )
{
  osimModel.realizeVelocity(osimState);

  bool isLocal = true;
  SimTK::Transform referential2ground;
  // find the frame associated 
  if (referential == "ground")
  {
    referential2ground = referential2ground.setToZero();
    isLocal = false;
  }
  else if (referential!="local")
  {
    osim::BodySet bodies = osimModel.upd_BodySet();
    int i = 0;
    while (i < bodies.getSize())
    {
      //std::cout << bodies[i].getName() << std::endl;
      if (bodies[i].getName() == referential)
      {
        referential2ground = osimModel.upd_BodySet()[i].getTransformInGround(osimState);
        i = bodies.getSize();
        isLocal = false;
      }
      i++;
    }
  }
  else
  {
    isLocal = true;
  }

  osim::MarkerSet markerSet = osimModel.upd_MarkerSet();
  std::map<std::string, Eigen::Vector3d> markersLocationNotRescaled;

  for (std::map<std::string, Eigen::Vector3d>::iterator it = markersLocation.begin(); it!= markersLocation.end(); it++)
  {   
        std::string body_name = D_markers_body[it->first];
        std::vector<double> stretch_factors;
        if (stretching_vector.size() > 1)
        {
          stretch_factors = stretching_vector[body_name];
        }
        else
        {
           std::map<std::string,std::vector<double> >::iterator it2 = stretching_vector.begin();
           stretch_factors = it2->second;
        }

        SimTK::Transform transf;
        if (isLocal)
        {
          // if local chosen, find the transformation between local_referential -> ground_referential
          int i =0;
          int index = 0;
          osim::Marker cur_mark= osimModel.upd_MarkerSet()[0];
          while (i < markerSet.getSize())
          {
            osim::Marker mark= osimModel.upd_MarkerSet()[i];
            if (mark.getName() == it->first)
            {
              cur_mark = mark;
              index = i;
              i = markerSet.getSize();
            }
            i++;

          }                    
          transf = osimModel.upd_MarkerSet()[index].getParentFrame().getTransformInGround(osimState);

        }

        else
        {
          transf = referential2ground;
        }

        SimTK::Vec4 q = transf.R().convertRotationToQuaternion().asVec4();
        Eigen::Quaternion<double> qe( q.get(0),q.get(1),q.get(2),q.get(3) );
        Eigen::Matrix3d R = qe.toRotationMatrix();
        Eigen::Matrix3d Rinv = R.inverse();

        SimTK::Vec3 pinvSim = transf.p();
        Eigen::Vector3d pinv(pinvSim.get(0), pinvSim.get(1), pinvSim.get(2));

        Eigen::Vector3d locationInFrame = Rinv*(it->second-pinv);// + pinv;
         //std::printf("Previous position : [%f,%f,%f] \n",locationInFrame[0],locationInFrame[1], locationInFrame[2]);
        locationInFrame[0] *= stretch_factors[0];
        locationInFrame[1] *= stretch_factors[1];
        locationInFrame[2] *= stretch_factors[2];
      // std::printf("New position computed : [%f,%f,%f] \n",locationInFrame[0],locationInFrame[1], locationInFrame[2]);
        Eigen::Vector3d newLoc = R*locationInFrame + pinv;
   
        markersLocation[it->first] = newLoc;

  }

}

/**
 * @brief Set markers positions, expressed in local bodies or ground referential.
 * 
 * By default, set markers positions in local bodies from ground positions of markers. We suppose model 
 * is already at a configuration that is matching the position of the markers.
 * 
 * @param markersLocation Position of the Opensim markers.
 * @param referential Name of the referential on which markers are expressed. This will be used to make the necessary
 * transformation, to express each marker into the local referential of the body to which it is associated.
 * Default value is "ground". This means that each marker is considered to be expressed in ground referential.
 * 
 */
void OsimModel::setMarkersPositions(std::map<std::string,Eigen::Vector3d> markersLocation,
std::string referential)
{

    osimModel.realizeVelocity(osimState);
    osim::MarkerSet markerSet = osimModel.upd_MarkerSet();

    for (std::map<std::string, Eigen::Vector3d>::iterator it = markersLocation.begin(); it!= markersLocation.end(); it++)
    {   
      SimTK::Transform transf;


          int i =0;
          int index = 0;
          osim::Marker cur_mark= osimModel.upd_MarkerSet()[0];
          while (i < markerSet.getSize())
          {
            osim::Marker mark= osimModel.upd_MarkerSet()[i];
            if (mark.getName() == it->first)
            {
              cur_mark = mark;
              index = i;
              i = markerSet.getSize();
            }
            i++;

          }
          if (referential == "ground")
          {
            transf = osimModel.upd_MarkerSet()[index].getParentFrame().getTransformInGround(osimState);
          }
          else if (referential == "local")
          {
            transf =transf.setToZero();
          }
          SimTK::Vec4 q = transf.R().convertRotationToQuaternion().asVec4();
          Eigen::Quaternion<double> qe( q.get(0),q.get(1),q.get(2),q.get(3) );
          Eigen::Matrix3d Rinv = qe.toRotationMatrix().inverse();

          SimTK::Vec3 pinvSim = transf.p();
          Eigen::Vector3d pinv(pinvSim.get(0), pinvSim.get(1), pinvSim.get(2));

          Eigen::Vector3d locationInFrame = Rinv*(it->second-pinv);// + pinv;
          SimTK::Vec3 locFrame = cur_mark.get_location();
          // std::printf("Previous position : [%f,%f,%f] \n",locFrame.get(0),locFrame.get(1), locFrame.get(2));
          locFrame.set(0,locationInFrame[0]);
          locFrame.set(1,locationInFrame[1]);
          locFrame.set(2,locationInFrame[2]);
          // if (referential == "local")
          // {
          //   std::printf("Name : %s. \n", it->first.c_str());
          //   std::printf("New position computed : [%f,%f,%f] \n",locFrame.get(0),locFrame.get(1), locFrame.get(2));
          // }

          //std::cout << osimModel.upd_MarkerSet()[index].getParentFrameName() << std::endl;
          //std::printf("New position computed : [%f,%f,%f] \n",locFrame.get(0),locFrame.get(1), locFrame.get(2));
          //osimModel.upd_MarkerSet()[index] = osim::Marker(it->first,osimModel.upd_MarkerSet()[index].getParentFrame(), locFrame);

          //osimModel.upd_MarkerSet()[index].upd_location() = locFrame;
          bool fixed = cur_mark.get_fixed();

          osimModel.upd_MarkerSet()[index].set_fixed(false);

          osimModel.upd_MarkerSet()[index].set_location(locFrame);

          osimModel.upd_MarkerSet()[index].set_fixed(fixed);

          SimTK::Vec3 checkLocFrame = osimModel.upd_MarkerSet()[index].get_location();


    }

  // necessary operation, in order for the Opensim model to make all computations with new markers
  double val = osimModel.updCoordinateSet()[0].getValue(osimState);
  osimModel.updCoordinateSet()[0].setValue(osimState,val,true);

  totalUpdModel();

}

/**
 * @brief Update variables that should be modified for each operation on the OsimModel object (ex : modify
 * the coordinates of the Opensim model).
 * 
 */
void OsimModel::updModel()
{
  D_joints_values = fosim::getJointsValues(osimModel,osimState);
  D_mark_pos = fosim::getMarkersPositions(osimModel,osimState);

}

/**
 * @brief Convenient method to change the values of the coordinates of the Opensim model.
 * Here, you just need to enter the couple Coordinate_name:Value to change it into the
 * Opensim model.
 * 
 * @param D_joints_values Name of the Coordinate of the Opensim model, and their values that should be modified.
 */
int OsimModel::setQ(std::map<std::string,double> D_joints_values, bool verbose)
{
  std::vector<double> q;

  for (std::map<std::string,double>::iterator it = D_joints_values.begin(); it!= D_joints_values.end(); it++)
  {
    q.push_back(it->second);
  }
  return( setQ(q,2, verbose) );
}

/**
 * @brief Convenient method to change the values of the coordinates of the Opensim model.
 * This method can have different types of coordinates values as input, indicated by the value
 * of the variable Qtyp : 
 *    - Qtyp = 0 : LQ vector as same order as Q vector of .osim
 *    - Qtyp = 1 : LQ vector as same order as Model.updCoordinateSet()
 *    - Qtyp = 2 : LQ vector in alphabetic order of the name of Coordinates. Typically appends when the Q vector was set thanks to D_joints_values.
 * 
 * @param LQ Vector containing the values that should be set to the Coordinates of the model.
 * @param Qtyp Variable indicating which is the order of the LQ vector. By default, its value is 1.
 */
  int OsimModel::setQ(std::vector<double> LQ, int Qtyp, bool verbose)
  {
        std::vector <double> RQ(LQ.size(), 0.0);
        if (Qtyp ==0)
        {
          
          for (int k = 0; k < LQ.size(); k++)
          {
            int coord_index = D_Q_coord[k];
            RQ[coord_index] = LQ[k];
          }

        }
        else if (Qtyp == 1)
        {
          RQ = LQ;
        }

        else if (Qtyp == 2)
        {
       
          for (int k = 0; k < LQ.size(); k++)
          {
            int mult_index = D_alph_multi[k];
            RQ[mult_index] = LQ[k];
          }

        }

        //set Q for the .osim model
        int result = setAndCorrectCoordVec(osimModel,osimState,RQ, verbose);

        //update the variables of the OsimModel class.
        updModel();

        return(result);

  }

  /**
   * @brief Convenient method to call to check if the current Coordinate Vector (q) has values
   *  consistent with the limits defined by the model.
   * 
   * @param osimModel 
   * @param osimState 
   * @param LQ 
   * @param correct_type 
   */
  int OsimModel::setAndCorrectCoordVec(osim::Model & osimModel, SimTK::State & osimState, std::vector<double> & LQ,  int correct_type, bool verbose)
  {
    std::map<std::string,double> prev_D_j_v = D_joints_values;
    int result = fosim::setAndCheckCoordVec(osimModel,osimState,LQ, correct_type, verbose);
    if (LQ.size() == 0)
    {
      osimState = osimModel.initSystem();
      setQ(prev_D_j_v, verbose);
    }
    return(result);
  }



  /**
   * @brief Convenient method to set only the joint between the ground referential and the root of the model.
   * (i.e. the orientation and global position of the subject in the global reference frame)
   * 
   * @param transf : Float 4x4 transformation matrix, containing the rotation and the translation matrix.
   */
  void OsimModel::setGroundJointOnly(Eigen::Matrix4f transf)
  {
        size_t NC = osimModel.getNumCoordinates();
        size_t NQ = osimState.getNQ();

       std::vector<double> q(NQ,0.0);
        std::map<std::string,int> D_gd_p = D_ground_props;

        // move the opensim model to the estimated position of the model

        Eigen::Vector3f trans = transf.block<3,1>(0,3);

        Eigen::Matrix3f rot = transf.block<3,3>(0,0);

        Eigen::Vector3f ea = (rot).eulerAngles(2,0,1);

        for (std::map<std::string,int>::iterator it=D_gd_p.begin(); it != D_gd_p.end(); it++)
        {
          if (it->first == "rot_x")
          {
            q[it->second] = ea[1];
          }
          if (it->first == "rot_y")
          {
            q[it->second] = ea[2];
          }
          if (it->first == "rot_z")
          {
            q[it->second] = ea[0];
          }
          if (it->first == "trans_x")
          {
            q[it->second] = trans[0];
          }
          if (it->first == "trans_y")
          {
            q[it->second] = trans[1];
          }
          if (it->first == "trans_z")
          {
            q[it->second] = trans[2];
          }

        }

        for (int i = 6; i < NC; i++)
        {
          q[i] = osimModel.updCoordinateSet()[i].getValue(osimState);
        }

        fosim::setAndCheckCoordVec(osimModel,osimState,q);
        //update the variables of the OsimModel class.
        updModel();

  }

  /**
   * @brief Convenient method to set the subject to its neutral pose. 
   * The values of this neutral pose correspond to the default values of the Coordinates of the model.
   * 
   */
  void OsimModel::setToNeutral()
  {
    fosim::setToNeutral(osimModel,osimState);
    updModel();
  }

  /**
   * @brief (Not functionnal for now) Get the Properties of Each Joint object (means : axis of rotation for each coordinate).
   * Idea : with this, be able to get coordinate value from a joint transformation. 
   * However, too much complicated; this idea is not exploited anymore.
   * 
   * @param osimModel 
   * @param osimState 
   */
  void getPropertiesOfEachJoint(osim::Model & osimModel, SimTK::State & osimState)
  {
    std::map<int,int> D_coord_Q =  getCorresQCoord(osimModel,osimState);
    osim::JointSet js = osimModel.upd_JointSet();
    //osim::Joint & gd_j = osimModel.upd_JointSet()[0];    
    osimModel.realizeVelocity(osimState);

    for (int i = 0; i < js.getSize(); i++)
    {
      osim::Joint & gd_j = osimModel.upd_JointSet()[i];      
      int jnc = gd_j.numCoordinates();
      std::cout << "joint name : " << gd_j.getName() << std::endl;
      for (int j =0 ; j < jnc; j++)
      {
        osim::Coordinate & coord = gd_j.upd_coordinates(j);
        int qmob = coord.getMobilizerQIndex();
        std::printf("Coord index : %d ; Q mob : %d; in D_coord_q : %d \n",j,qmob,D_coord_Q[0]);
        SimTK::Transform transform = gd_j.get_frames(1).findTransformBetween(osimState, gd_j.get_frames(0)  );
        std::cout << transform << std::endl;
        coord.setValue(osimState,1.0,true);
        SimTK::Transform t2 = gd_j.get_frames(1).findTransformBetween(osimState, gd_j.get_frames(0) );
        std::cout << t2 << std::endl;
        SimTK::Vec4 q = t2.R().convertRotationToQuaternion().asVec4();
        Eigen::Quaternion<double> qe( q.get(0),q.get(1),q.get(2),q.get(3) );
        Eigen::Vector3d ea = qe.toRotationMatrix().eulerAngles(0, 1, 2) ;        
        std::cout << "ea : " << ea << std::endl;
        coord.setValue(osimState,0.0,true);
      }      

    }
  }

/**
 * @brief (Not functionnal for now) Trial to set q vector from a transfrom, thanks to setQToFitTransform
 * ( in https://simtk.org/api_docs/simbody/api_docs32/Simbody/html/classSimTK_1_1MobilizedBody.html#a5f926e05e89b9d8a157272b1512b32c9);
 * Not working for now : bug :  what():  ModelComponent::updModel(): component 'torso_offset' of type PhysicalOffsetFrame does not belong 
 * to a model. Have you called Model::initSystem()?
 * 
 * @param osimModel 
 * @param osimState 
 */
void setQWithTransform(osim::Model & osimModel, SimTK::State & osimState)
{
    std::map<std::string,std::string> D_child_parent;

  osim::JointSet js = osimModel.upd_JointSet();

  SimTK::State s2 = osimModel.updWorkingState();

  for (int i = 1; i < js.getSize(); i++)
  {
    std::string phys_name = js[i].get_frames(0).getName();

    std::string connect_parent_path = js[i].upd_frames(0).getSocket<osim::PhysicalFrame>("parent").getConnecteePath();
    std::cout << js[i].upd_frames(0).updMobilizedBody().getNumQ(s2) << std::endl;
    std::vector<std::string> list = splitString(connect_parent_path,"/");
    std::string body_parent_name = list[list.size()-1];

    std::string connect_child_path = js[i].upd_frames(1).getSocket<osim::PhysicalFrame>("parent").getConnecteePath();
    list = splitString(connect_child_path,"/");
    std::string body_child_name = list[list.size()-1];


    D_child_parent[body_child_name] = body_parent_name;

  }
}
/**
 * @brief (Not functionnal for now) Second try, inspired from https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_b-examples_i-inverse-kinematics.html
 * 
 * @param osimModel 
 * @param osimState 
 */
void setQWithTransform2(osim::Model & osimModel, SimTK::State & osimState)
{
    std::map<std::string,std::string> D_child_parent;

  osim::JointSet js = osimModel.upd_JointSet();

  SimTK::State s2 = osimModel.updWorkingState();

  for (int i = 1; i < js.getSize(); i++)
  {
    std::string phys_name = js[i].get_frames(0).getName();

    std::string connect_parent_path = js[i].upd_frames(0).getSocket<osim::PhysicalFrame>("parent").getConnecteePath();
    std::cout << js[i].upd_frames(0).updMobilizedBody().getNumQ(s2) << std::endl;
    std::vector<std::string> list = splitString(connect_parent_path,"/");
    std::string body_parent_name = list[list.size()-1];

    std::string connect_child_path = js[i].upd_frames(1).getSocket<osim::PhysicalFrame>("parent").getConnecteePath();
    list = splitString(connect_child_path,"/");
    std::string body_child_name = list[list.size()-1];


    D_child_parent[body_child_name] = body_parent_name;

  }
}
/**
 * @brief Method returning, for each body of the model, its parent.
 * 
 * @param osimModel 
 * @return std::map<std::string,std::string> 
 */
std::map<std::string,std::string> getChildParentBody(osim::Model & osimModel)
{
  std::map<std::string,std::string> D_child_parent;

  osim::JointSet js = osimModel.upd_JointSet();

  for (int i = 0; i < js.getSize(); i++)
  {
    std::string phys_name = js[i].get_frames(0).getName();



    std::string connect_parent_path = js[i].upd_frames(0).getSocket<osim::PhysicalFrame>("parent").getConnecteePath();
   
    std::vector<std::string> list = splitString(connect_parent_path,"/");
    std::string body_parent_name = list[list.size()-1];

    std::string connect_child_path = js[i].upd_frames(1).getSocket<osim::PhysicalFrame>("parent").getConnecteePath();
    list = splitString(connect_child_path,"/");
    std::string body_child_name = list[list.size()-1];


    D_child_parent[body_child_name] = body_parent_name;

  }
  
  return(D_child_parent);


}
/**
 * @brief Method displaying, for each body of the model, its parent. Based on the
 * method getChildParentBody.
 * 
 * @param osimModel 
 */
void printChildParentBody(osim::Model & osimModel)
{
  std::map<std::string,std::string> D_child_parent = getChildParentBody(osimModel);

  for (std::map<std::string,std::string>::iterator it = D_child_parent.begin(); it!=D_child_parent.end(); it++)
  {
    std::printf("Body child : %s ; Body parent : %s \n", it->first.c_str(), it->second.c_str() );
  }

}

/**
 * @brief Get the properties of the joint between the ground and the root link of the model.
 * 
 * @param osimModel 
 * @param osimState 
 * @return std::map<std::string,int> Dictionnary containing the type of transformation and its index into the Q vector of the model.
 * Ex : rot_x:6 ; the rotation around the axis x corresponds to the element of index 6 in the Q vector.
 * trans_z:2 : the translation across the axis z corresponds to the element of index 2 in the Q vector.
 * 
 */
std::map<std::string,int> getGroundProperties(osim::Model & osimModel, SimTK::State & osimState)
{
  
  // D[rot_x] = index_q
  // D[trans_y] = index_q
  std::map<std::string,int> D_gd_prop;

  osim::JointSet js = osimModel.upd_JointSet();
  
  osim::Joint & gd_j = osimModel.upd_JointSet()[0];

  int gd_ind = -1;
  int i =0;

  while( i < js.getSize())
  {
    std::string phys_name = js[i].get_frames(0).getName();
    if (phys_name.find("ground") != -1)
    {
      gd_j = osimModel.upd_JointSet()[i];
      i = js.getSize();
    }
    i++;
  }

  if ( i == js.getSize() +1 )
  {

      size_t NC = osimModel.getNumCoordinates();
      std::vector<double> gd_ori_values;
      //std::cout << gd_j.getName() << std::endl;
      for (int j =0 ; j < 6; j++)
      {
        gd_ori_values.push_back(osimModel.updCoordinateSet()[j].getValue(osimState));
        osimModel.updCoordinateSet()[j].setValue(osimState,0.0,true);
      }
     
      for (int j =0 ; j < 6; j++)
      {

        osim::Coordinate & coord = gd_j.upd_coordinates(j);

        osimModel.updCoordinateSet()[j].setValue(osimState,0.3,true);
        std::string trans_type;
        SimTK::Transform transform = gd_j.get_frames(1).getTransformInGround(osimState);



        if (coord.getMotionType() == osim::Coordinate::MotionType::Translational )
        {
/*             std::cout << "type trans " <<std::endl;
            std::cout << "name : " << coord.getName() << std::endl;
            std::cout << "index : " << coord.getMobilizerQIndex() << std::endl;
            std::cout << "j : " << j << std::endl; */

            SimTK::Vec3 Tt = transform.T();

            if ( Tt.get(0) == 0.3)
            {
              trans_type = "trans_x";
            }
            else if ( Tt.get(1) ==0.3)
            {
              trans_type = "trans_y";
            }
            else if ( Tt.get(2) == 0.3)
            {
              trans_type = "trans_z";
            }

        }
        else if (coord.getMotionType() == osim::Coordinate::MotionType::Rotational )
        {
            SimTK::Vec4 q = transform.R().convertRotationToQuaternion().asVec4();
            Eigen::Quaternion<double> qe( q.get(0),q.get(1),q.get(2),q.get(3) );
            Eigen::Vector3d ea = qe.toRotationMatrix().eulerAngles(0, 1, 2) ;
            if ( abs( ea[0]-0.3) <1e-2 ) 
            {
              trans_type = "rot_x";
            }
            else if ( abs( ea[1]-0.3) <1e-2 ) 
            {
              trans_type = "rot_y";
            }
            else if ( abs( ea[2]-0.3) <1e-2 ) 
            {
              trans_type = "rot_z";
            }

        }

        D_gd_prop[trans_type] = j;
        osimModel.updCoordinateSet()[j].setValue(osimState,0.0,true);

      }
      for (int j =0 ; j < 6; j++)
      {
        osimModel.updCoordinateSet()[j].setValue(osimState,gd_ori_values[j],true);
      }      

  } 

return(D_gd_prop);

}
/**
 * @brief Print the properties of the joint between the ground and the root link of the model.
 * Based on getGroundProperties.
 * 
 * @param osimModel 
 * @param osimState 
 */
void printGroundProperties(osim::Model & osimModel, SimTK::State & osimState)
{
  std::map<std::string,int> D_gd_prop = getGroundProperties(osimModel,osimState);

  printf(" ---- Ground Joint properties ---- \n");
  for (std::map<std::string, int>::iterator it = D_gd_prop.begin(); it != D_gd_prop.end(); it++)
  {
    printf("Type of rotation : %s ; coordinateIndex : %i \n", it->first.c_str(), it->second);
  }
  
}

/**
 * @brief Get the Opensim marker of the model which is the closest to the origin of the referential in which the
 * markers are expressed.
 * 
 * @param osimModel 
 * @param osimState 
 * @return Eigen::Vector3d 
 */
  Eigen::Vector3d getLowestOsimMarker(osim::Model & osimModel, SimTK::State & osimState)
  {

    std::map<std::string, Eigen::Vector3d> D_markers_positions = getMarkersPositions(osimModel,osimState);

    std::vector<Eigen::Vector3d> positions;
  
    for (std::map<std::string, Eigen::Vector3d>::iterator it = D_markers_positions.begin(); it!=D_markers_positions.end(); it++)
    {
      positions.push_back(it->second);
    }
  
  Eigen::Vector3d lowest_mark = getLowestMarker(positions);

  return(lowest_mark);

  }
/**
 * @brief Get for each Joint of the model its transformation from a specified referential refe to one of 
 * the two referentials of the joint (the referential associated to the parent body of the joint, or the 
 * referential associated with the child body of the joint).
 * 
 * @param osimModel 
 * @param osimState 
 * @param refe 
 * @param n : Number associated with the joint referential (0 : parent; 1 : child).
 * @return std::map<std::string, SimTK::Transform > 
 */
std::map<std::string, SimTK::Transform > getJointsTransform(osim::Model & osimModel, SimTK::State & osimState, std::string refe, int n)
{
  std::map< std::string, SimTK::Transform >  D_joints_transform;
  for (int i =0; i< osimModel.getNumJoints(); i++)
  {
    osim::Joint & joint = osimModel.upd_JointSet()[i];

    std::string name = joint.getName();
    //osim::PhysicalFrame p_frame = joint.get_frames(n);
    SimTK::Transform transf;
    if (refe == "Ground")
    {
       transf = joint.get_frames(n).getTransformInGround(osimState);
    }
    else if (refe == "Parent")
    {
      //osim::PhysicalFrame pp_frame = joint.get_frames(n).getParentFrame();
      transf = joint.get_frames(n).findTransformBetween(osimState, joint.get_frames(n).getParentFrame()  );
    }
    else if (refe == "Child")
    {
      //osim::PhysicalFrame pp_frame = joint.get_frames(n).getParentFrame();
      transf = joint.getChildFrame().findTransformBetween(osimState, joint.get_frames(n)  );
    }    
    D_joints_transform[joint.getName()] = transf;

  }
  return(D_joints_transform);
}

/**
 * @brief Convenient method to split a string, in a similar way than the Python method split().
 * 
 * @param str 
 * @param delimiter 
 * @return std::vector<std::string> 
 */
  std::vector<std::string> splitString(std::string str, std::string delimiter)
  {
    std::vector<std::string> list;
    while (str.find(delimiter) != -1)
    {
      std::string token = str.substr(0, str.find(delimiter));
      list.push_back(token);
      str.erase(0, str.find(delimiter) + delimiter.length());

    }
  list.push_back(str);
  return(list);

  }
  /**
   * @brief Get, for each body of the model, the names of all the markers attributed to this body.
   * (NB : if a body has no markers, it won't be present into this object).
   * 
   * @param osimModel 
   * @param osimState 
   * @return std::map<std::string, std::vector<std::string> > 
   */
  std::map<std::string, std::vector<std::string> > getMarkersByBodyParts(osim::Model & osimModel,SimTK::State & osimState)
  {
      osimModel.realizeVelocity(osimState);
      osim::MarkerSet markerSet = osimModel.upd_MarkerSet();
      std::map<std::string, std::vector<std::string> >D_body_markers;

      for (int i =0; i< markerSet.getSize(); i++)
      {   
          std::string name = osimModel.get_MarkerSet()[i].getName();
          std::string phys_name =  osimModel.get_MarkerSet()[i].getParentFrameName();
          std::vector<std::string> list = splitString(phys_name,"/");
          std::string frame_name = list[list.size()-1];

          if ( D_body_markers.find(frame_name) == D_body_markers.end())
          {
            D_body_markers[frame_name] = std::vector<std::string>();
          }
          D_body_markers[frame_name].push_back(name);
      }
      return(D_body_markers);

  }
/**
 * @brief Get, for each Marker, the body to which it is attached.
 * 
 * @param osimModel 
 * @param osimState 
 * @return std::map<std::string, std::string > 
 */
  std::map<std::string, std::string > getBodyByMarkers(osim::Model & osimModel,SimTK::State & osimState)
  {
      osimModel.realizeVelocity(osimState);
      osim::MarkerSet markerSet = osimModel.upd_MarkerSet();
      std::map<std::string, std::string> D_markers_body;

      for (int i =0; i< markerSet.getSize(); i++)
      {   
          std::string name = osimModel.get_MarkerSet()[i].getName();
          std::string phys_name =  osimModel.get_MarkerSet()[i].getParentFrameName();
          std::vector<std::string> list = splitString(phys_name,"/");
          std::string frame_name = list[list.size()-1];

          D_markers_body[name] = frame_name;
      }
      return(D_markers_body);

  }

/**
 * @brief Get the Opensim marker which position is the closest of the origin of the
 * referential in which the markers are expressed.
 * 
 * @param positions 
 * @return Eigen::Vector3d 
 */
  Eigen::Vector3d getLowestMarker(std::vector<Eigen::Vector3d> positions)
  {
    double min_dist = 9999;
    Eigen::Vector3d lowest_mark;
  
    for (int i =0; i<positions.size(); i++)
    {
      Eigen::Vector3d mark1 = positions[i];
      //double dist = mark1.norm();
      double dist = mark1[1];
      if (dist < min_dist ) 
      {
          min_dist = dist;
          lowest_mark = mark1;
      } 
    }

   return(lowest_mark);

  }

/**
 * @brief Get the values of each of the coordinates of the model, with pairs name_of_coordinate:value_of_coordinate.
 * 
 * @param Model 
 * @param osimState 
 * @return std::map<std::string, double> 
 */
  std::map<std::string, double> getJointsValues(osim::Model & Model, SimTK::State & osimState)
  {
    std::map<std::string, double> D_joint_value;
    size_t NC = Model.getNumCoordinates();
    for (int i = 0; i < NC; i++)
    {
      std::string name = Model.updCoordinateSet()[i].getName();
      double coordval =  Model.updCoordinateSet()[i].getValue(osimState);
      D_joint_value[name] = coordval;

    }

  return D_joint_value;
  }

/**
 * @brief Get the correspondance between the index of the coordinates into a map object,
 * and the index of the coordinates repertoried into updCoordinateSet().
 * The main reason of this adding was that sometimes, the user was giving the values for
 * the Q vector according to the order of the values of the joint displayed by the function printJointValues.
 * Unfortunately, as those values are displayed by alphabetic order, they do not match with the order of updCoordinateSet().
 * Thus, the values given by the user were false. 
 * 
 * @param osimModel 
 * @param osimState 
 * @return std::map<int, int> 
 */
std::map<int, int> getCorresAlphMulti(osim::Model & osimModel,SimTK::State & osimState)
{
    size_t NC = osimModel.getNumCoordinates();

    std::map<std::string, int> coords;

    for (int i =0; i< NC; i++ )
    {
      
      std::string name = osimModel.updCoordinateSet()[i].getName();
      coords[name] = i;
    }
    
    std::map<int, int> D_alph_multi;

    int j = 0;
    for (std::map<std::string, int>::iterator it = coords.begin(); it != coords.end(); it++)
    {
      D_alph_multi[j] = it->second;
      j++;

    }
    
    return(D_alph_multi);
};

/**
 * @brief Print the correspondance between the index of the coordinates into a map object,
 * and the index of the coordinates repertoried into updCoordinateSet(). Based on getCorresAlphMulti().
 * 
 * @param osimModel 
 * @param osimState 
 */
void printCorresAlphMulti(osim::Model & osimModel,SimTK::State & osimState)
{
    std::map<int,int> D_alph_multi = getCorresAlphMulti(osimModel,osimState);
    size_t NC = osimModel.getNumCoordinates();
    int i = 0;
    for (std::map<int,int>::iterator it = D_alph_multi.begin(); it!=D_alph_multi.end(); it++)
    {
      std::string name = osimModel.updCoordinateSet()[it->second].getName();
      printf("Index of %s (index in D_alph_multi : %d) : in D_joint_values : %d; in updCoordinateSet : %d. \n",name.c_str(),i,it->first,it->second);
      i++;
      
    }
    
};

/**
 * @brief Get the correspondences between the index of the Q vector of the Opensim model and the index of
 * the Coordinates of the model, repertoried in updCoordinateSet().
 * 
 * @param osimModel 
 * @param osimState 
 * @return std::map<int, int> 
 */
std::map<int, int> getCorresQCoord(osim::Model & osimModel,SimTK::State & osimState)
{
    size_t NC = osimModel.getNumCoordinates();
     
    std::map<int, int> D_Q_coord;
     
    size_t NQ = osimState.getNQ();
     
    std::vector<double> LQ(NQ,0.0);
    for (int i =0; i<NQ; i++ )
      {
        LQ[i] = 0.25;
        fosim::setAndCheckCoordVec(osimModel,osimState,LQ);
        int j  = 0;
        for (j =0; j<NC; j++ )
        {
            double val = osimModel.updCoordinateSet()[j].getValue(osimState);
            if (val == 0.25)
              {
                break;
              }
        }
        D_Q_coord[i] = j;
        LQ[i] = 0.0;
      }
    
    return(D_Q_coord);
};

/**
 * @brief Get the correspondences between the index of the Coordinates of the model, repertoried in 
 * updCoordinateSet(), and the index of the Q vector of the Opensim model. 
 * This method is the inverse method of getCorresQCoord().
 * 
 * @param osimModel 
 * @param osimState 
 * @return std::map<int, int> 
 */
std::map<int, int> getCorresCoordQ(osim::Model & osimModel,SimTK::State & osimState)
{
    size_t NC = osimModel.getNumCoordinates();
    size_t NQ = osimState.getNQ();
     
    std::map<int, int> D_coord_Q;

     
    std::vector<double> LC(NC,0.0);
    for (int i =0; i<NQ; i++ )
      {
        LC[i] = 0.25;
        fosim::setAndCheckCoordVec(osimModel,osimState,LC);
        int j  = 0;
        SimTK::Vector Q = osimState.getQ();
        for (j =0; j<NQ; j++ )
        {
            double val = Q[j];
            if (val == 0.25)
              {
                break;
              }
        }
        D_coord_Q[i] = j;
        LC[i] = 0.0;
      }
    return(D_coord_Q);
};

/**
 * @brief Print the values of each of the coordinates of the model, with pairs name_of_coordinate:value_of_coordinate.
 * Based on the method getJointsValue().
 * Please note that the joints values are printed in the same order as the order of updCoordinateSet, for simplicity.
 * 
 * @param Model 
 * @param osimState 
 */
  void printJointsValues(osim::Model & Model, SimTK::State & osimState)
  {
    printf(" ---- Joints Values ---- \n");
    std::map<std::string, double> D_joint_value = getJointsValues(Model,osimState);
    osim::CoordinateSet coords = Model.updCoordinateSet();
    for (int i = 0; i < coords.getSize(); i++)
    {
      std::string name = (coords[i]).getName();
      //std::string name = (*coords[i]).getName();
      printf("Name %s : value : %f \n",name.c_str(),D_joint_value[name]);
    }
  printf(" -------- \n");
  }


  /**
   * @brief Applies a possible correction to the values ordered at the LQ joints, then sets these values for the Opensim model.
   * Currently, 2 types of correction are implemented : 
   * - correction by number of revolutions (correct_type = 0): if the given value exceeds the limits of the joint, it is considered that this value corresponds in fact to a "revolution"; i.e. an expression of the value in relation to the second bound. 
   *   E.g.: value = pi + 0.1, upper bound to pi, lower bound to -pi; the corrected value is then -pi + 0.1 (= as if a revolution was made).
   *   The results of this correction are, however, disappointing in practice.
   *
   * - saturation correction (correct_type = 1): the value is simply saturated according to the upper and lower bounds. 
   * 
   * By default, correct_type = 1 is set. 
   *
   * @param osimModel 
   * @param osimState 
   * @param LQ Vector contaning all the values for the Coordinate of the OpensimModel.
   * Please note that those values should be set accordingly to the order in the updCoordinateSet() object of the Opensim Model.
   * (NB : normally, this operation is made by other methods called previously).
   * @param correct_type 
   * @return result : 
   *  =-1 : IK failed
   * = 0 : succeded, but with correction
   * = 1 : succeded without issues
   */
  int setAndCheckCoordVec(osim::Model & osimModel, SimTK::State & osimState, std::vector<double> & LQ,  int correct_type, bool verbose)
  {

    int result = 1;

    if (LQ.size() != osimModel.getNumCoordinates())
    {
      printf("WARNING : The proposed Q vector does not have the same size (= %li) than the Q vector of the Opensim Model ( = %i). Cancelling the operation.\n",LQ.size(),osimModel.getNumCoordinates());
      result = -1;
    }
    else
    {
      bool need = false;

      int NC = osimModel.getNumCoordinates();
      for (int k = 0; k< NC; k++)
      {
        bool already = false;
        
        double min_range = osimModel.updCoordinateSet()[k].get_range(0);
        double max_range = osimModel.updCoordinateSet()[k].get_range(1);
        double current_val = LQ[k];
        std::string name = osimModel.updCoordinateSet()[k].getName();
        if (current_val > max_range)
        {
          already = true;
          if (verbose)
          {
            std::printf("Value in max (name : %s) : %3.5f; min range : %3.5f; max range : %3.5f. \n", name.c_str(), current_val, min_range, max_range);
          }
        }
        else if (current_val < min_range)
        {
          already = true;
          if (verbose)
          {
            std::printf("Value in min (name : %s) : %3.5f; min range : %3.5f; max range : %3.5f. \n", name.c_str(), current_val, min_range, max_range);
          }
        }

        if (correct_type == 0)
        {
          if (current_val > max_range)
          {
            current_val = std::fmod(current_val,max_range)+ min_range;
          }
          else if (current_val < min_range)
          {
            current_val = std::fmod(current_val,min_range) + max_range;
          }
        }
        else if (correct_type == 1)
        {
          double val = std::min( std::max(min_range,current_val), max_range);
          current_val = val;
        }

        if (already)
        {
          if (verbose)
          {
          std::printf("After value : %3.5f \n", current_val);
          }
          need = true;
          result = 0;
        }        

        osimModel.updCoordinateSet()[k].setValue(osimState,current_val,false);      
        
      }
      osimModel.assemble(osimState);
      if (need) {  
        LQ.clear();
        if (verbose)
        {
        std::cout << "--end of correction--" << std::endl; }
        }
    }

    return(result);

  }

/**
 * @brief Set Q vector instead of coordinates. Not working for now for unknown reasons. 
 * terminate called after throwing an instance of 'SimTK::Exception::IndexOutOfRange'
  what():  SimTK Exception thrown at StateImpl.h:964:
  Index out of range in StateImpl::getSubsystem(): expected 0 <= subx < 5 but subx=496.
  Even weirder : if the commented area is uncommented, the globality of the OsimModel class
  stops working and returns the previous error, even if setQVec is not called at all anywhere in
  this file.
 * 
 * @param osimModel 
 * @param osimState 
 * @param LQ 
 */
  void setQVec(osim::Model & osimModel, SimTK::State & osimState, std::vector<double> LQ)
  {

    if (LQ.size() != osimState.getNQ())
    {
      printf("WARNING : The proposed Q vector does not have the same size (= %li) than the Q vector of the Opensim Model ( = %i). Cancelling the operation.\n",LQ.size(),osimState.getNQ());
    }
    else
    {
      std::cout << "here" << std::endl;
/*       osimModel.realizeVelocity(osimState);
      int k = osimState.getNumSubsystems();
      SimTK::Vector Q = osimState.updQ();
      for (int i =0; i < LQ.size(); i++)
      {
        Q.set(i, LQ[i]);
      }
      osimState.setNumSubsystems(k);
      osimState.setQ(Q);
      osimModel.assemble(osimState); */

    }

  }

  /**
   * @brief Method setting the model to a neutral position.
   * 
   * @param Model 
   * @param osimState 
   */
  void setToNeutral(osim::Model & Model, SimTK::State & osimState)
  {
    std::vector<double> q;
    osim::CoordinateSet cs = Model.getCoordinateSet();

    for (int k = 0; k< cs.getSize(); k++)
    {
        q.push_back(cs[k].getDefaultValue());
    }
    setAndCheckCoordVec(Model, osimState, q);

  }

  /**
   * @brief Get the position of the markers of the model in a specified referential refe.
   * By default, the referential is "Ground".
   * @param osimModel 
   * @param osimState 
   * @param refe Name of the referential in which the markers should be expressed. This referential can be : 
   * - Ground : the global referential of the model.
   * - Local : the referential of the body part to which the model is attached.
   * @return std::map<std::string, Eigen::Vector3d> 
   */
  std::map<std::string, Eigen::Vector3d> getMarkersPositions(osim::Model & osimModel, SimTK::State & osimState, std::string refe)
  {  
    osimModel.realizeVelocity(osimState);
    osim::MarkerSet markerSet = osimModel.upd_MarkerSet();
    std::map<std::string, Eigen::Vector3d>D_markers;

    for (int i =0; i< markerSet.getSize(); i++)
    {   
        std::string name = osimModel.upd_MarkerSet()[i].getName();
        SimTK::Vec3 pos;
        if (refe == "Ground")
        {
          pos =  osimModel.upd_MarkerSet()[i].getLocationInGround(osimState);
        }
        else if (refe == "Local")
        {
          pos = osimModel.upd_MarkerSet()[i].get_location();
        }
        Eigen::Vector3d position; position <<  pos.get(0), pos.get(1), pos.get(2);
        D_markers[name] = position;
    }
    return(D_markers);
  }

/**
 * @brief For each Opensim markers, get the distances between this marker and the other markers on the same body.
 * 
 * @param osMod 
 * @param markers_positions 
 * @return std::map<std::string, std::map<std::string,double> > 
 */
  std::map<std::string, std::map<std::string,double> > getMarkerDistancesInBody(OsimModel & osMod, std::map<std::string, Eigen::Vector3d> markers_positions)
  {

      osim::Model osimModel = osMod.osimModel;
      SimTK::State osimState = osMod.osimState;
      //osimModel.realizeVelocity(osimState);
      osim::MarkerSet markerSet = osimModel.upd_MarkerSet();
      std::map<std::string, std::map<std::string, double> > D_markers_all_dist;

      std::map<std::string, std::vector<int> > markers_by_frame;
      for (int i =0; i< markerSet.getSize(); i++)
      {   
          std::string frame_name = osimModel.upd_MarkerSet()[i].getParentFrameName();
          if (  markers_by_frame.find( frame_name) == markers_by_frame.end() )
          {
            markers_by_frame[frame_name] = std::vector<int>();
          }
          markers_by_frame[frame_name].push_back(i);
      }

      std::map<std::string,double> cur_D_markers_dist_body;

      for ( std::map<std::string, std::vector<int> >::iterator it = markers_by_frame.begin(); it!= markers_by_frame.end(); it++)
      {   
          std::vector<int> all_index = it->second;
          // get min dist for each marker of body part
          for (int l = 0; l < all_index.size(); l++ )
          {

            double min_dist = 9999;
            int i = all_index[l];

            std::map<std::string,double> D_markers_dist_body_i;
            std::string name_i = osimModel.upd_MarkerSet()[i].getName();

            if (D_markers_all_dist.find(name_i)!= D_markers_all_dist.end())
            {
              D_markers_dist_body_i = D_markers_all_dist[name_i];
            }

            Eigen::Vector3d position = markers_positions[name_i]; 
            for (int k = 0; k< all_index.size(); k++)
            {
              int j = all_index[k];
              std::string name_j = osimModel.upd_MarkerSet()[j].getName();
              std::map<std::string,double> D_markers_dist_body_j;

              if (name_i != name_j)
              {
                if (D_markers_all_dist.find(name_j)!= D_markers_all_dist.end())
                {
                  D_markers_dist_body_j = D_markers_all_dist[name_j];
                }
                // means the distance name_i <-> name_j has not been computed yet
                if (D_markers_dist_body_j.find(name_i) == D_markers_dist_body_j.end())
                {

                  Eigen::Vector3d position_j = markers_positions[name_j];
                  double dist = (position_j-position).norm();
                  D_markers_dist_body_i[name_j] = dist;
                  D_markers_dist_body_j[name_i] = dist;

                  D_markers_all_dist[name_j] = D_markers_dist_body_j;
                }
              }
            }
            
            D_markers_all_dist[name_i] = D_markers_dist_body_i;
          }

      }
    return(D_markers_all_dist);

  } 

  /**
   * @brief  For each Opensim markers, print the distances between this marker and the other markers on the same body. Based on getMarkerDistancesInBody.
   * 
   * @param osMod 
   * @param markers_positions 
   */
  void printMarkerDistancesInBody(OsimModel & osMod, std::map<std::string, Eigen::Vector3d> markers_positions)
  {
    
    std::map<std::string, std::map<std::string,double> > D_mark_all_dist = getMarkerDistancesInBody( osMod,markers_positions);
    for (std::map<std::string, std::map<std::string,double> >::iterator it = D_mark_all_dist.begin(); it!= D_mark_all_dist.end(); it++)
    {
      std::string name_mark = it->first;
      std::map<std::string,double>  dist_mark = it->second;
      std::printf("Marker  : %s \n", name_mark.c_str() );
      for ( std::map<std::string,double>::iterator it2 = dist_mark.begin(); it2!= dist_mark.end(); it2++)
      {
        std::printf("--- Distance from : %s : %f \n",it2->first.c_str(), it2->second);
      }
      std::printf("----");

    }


  }
  /**
   * @brief  A more convenient method to print the distances between this marker and the other each markers of the same body. Based on getMarkerDistancesInBody.
   * 
   * 
   * @param osimModel 
   * @param osimState 
   */
  void printMarkerDistancesInBody(osim::Model & osimModel, SimTK::State & osimState)
  {
    
    std::map<std::string, std::map<std::string,double> > D_mark_all_dist = getMarkerDistancesInBody(osimModel, osimState);
    for (std::map<std::string, std::map<std::string,double> >::iterator it = D_mark_all_dist.begin(); it!= D_mark_all_dist.end(); it++)
    {
      std::string name_mark = it->first;
      std::map<std::string,double>  dist_mark = it->second;
      std::printf("Marker  : %s \n", name_mark.c_str() );
      for ( std::map<std::string,double>::iterator it2 = dist_mark.begin(); it2!= dist_mark.end(); it2++)
      {
        std::printf("--- Distance from : %s : %f \n",it2->first.c_str(), it2->second);
      }
      std::printf("----");

    }
  }

/**
 * @brief  A more convenient method to get the distances between this marker and the other each markers of the same body. Based on getMarkerDistancesInBody.
 * 
 * @param osimModel 
 * @param osimState 
 * @return std::map<std::string, std::map<std::string,double> > 
 */
std::map<std::string, std::map<std::string,double> > getMarkerDistancesInBody(osim::Model & osimModel, SimTK::State & osimState)
{
      osimModel.realizeVelocity(osimState);
      osim::MarkerSet markerSet = osimModel.upd_MarkerSet();
      std::map<std::string, std::map<std::string, double> > D_markers_all_dist;

      
      std::map<std::string, std::vector<int> > markers_by_frame;
      for (int i =0; i< markerSet.getSize(); i++)
      {   
          std::string frame_name = osimModel.upd_MarkerSet()[i].getParentFrameName();
          if (  markers_by_frame.find( frame_name) == markers_by_frame.end() )
          {
            markers_by_frame[frame_name] = std::vector<int>();
          }
          markers_by_frame[frame_name].push_back(i);
      }


      for ( std::map<std::string, std::vector<int> >::iterator it = markers_by_frame.begin(); it!= markers_by_frame.end(); it++)
      {   
          std::vector<int> all_index = it->second;
          // get min dist for each marker of body part
          for (int l = 0; l < all_index.size(); l++ )
          {
            std::map<std::string,double> D_markers_dist_body;

            double min_dist = 9999;
            int i = all_index[l];
            std::string name = osimModel.upd_MarkerSet()[i].getName();
            SimTK::Vec3 pos =  osimModel.upd_MarkerSet()[i].getLocationInGround(osimState);

            Eigen::Vector3d position; position <<  pos.get(0), pos.get(1), pos.get(2);
            for (int k = 0; k< all_index.size(); k++)
            {
              int j = all_index[k];
              if (k!=l)
              {
                SimTK::Vec3 pos_j =  osimModel.upd_MarkerSet()[j].getLocationInGround(osimState);
                Eigen::Vector3d position_j; position_j <<  pos_j.get(0), pos_j.get(1), pos_j.get(2);
                double dist = (position_j-position).norm();
                D_markers_dist_body[osimModel.upd_MarkerSet()[j].getName()] = dist;


              }
            }
            
            D_markers_all_dist[name] = D_markers_dist_body;
          }

      }
    return(D_markers_all_dist);

} 


  /**
   * @brief Get the closest distance between each marker of a body and the other markers of the same body part.
   * 
   * @param osimModel 
   * @param osimState 
   * @return std::map<std::string, double> 
   */
  std::map<std::string, double> getClosestMarkerInBody(osim::Model & osimModel, SimTK::State & osimState)
  {
        osimModel.realizeVelocity(osimState);
        osim::MarkerSet markerSet = osimModel.upd_MarkerSet();
        std::map<std::string,double>D_markers_body_closest;

        
        std::map<std::string, std::vector<int> > markers_by_frame;
        for (int i =0; i< markerSet.getSize(); i++)
        {   
            std::string frame_name = osimModel.upd_MarkerSet()[i].getParentFrameName();
            if (  markers_by_frame.find( frame_name) == markers_by_frame.end() )
            {
              markers_by_frame[frame_name] = std::vector<int>();
            }
            markers_by_frame[frame_name].push_back(i);
        }


        for ( std::map<std::string, std::vector<int> >::iterator it = markers_by_frame.begin(); it!= markers_by_frame.end(); it++)
        {   

            std::vector<int> all_index = it->second;
            // get min dist for each marker of body part
            for (int l = 0; l < all_index.size(); l++ )
            {
              
              double min_dist = 9999;
              int i = all_index[l];
              std::string name = osimModel.upd_MarkerSet()[i].getName();
              SimTK::Vec3 pos =  osimModel.upd_MarkerSet()[i].getLocationInGround(osimState);

              Eigen::Vector3d position; position <<  pos.get(0), pos.get(1), pos.get(2);
              for (int j = 0; j< all_index.size(); j++)
              {
                if (j!=l)
                {
                  SimTK::Vec3 pos_j =  osimModel.upd_MarkerSet()[j].getLocationInGround(osimState);
                  Eigen::Vector3d position_j; position_j <<  pos_j.get(0), pos_j.get(1), pos_j.get(2);
                  double dist = (position_j-position).norm();
                  if (dist < min_dist)
                  {
                    min_dist = dist;
                  }

                }
              }
                
              D_markers_body_closest[name] = min_dist;
            }

        }
      return(D_markers_body_closest);
  } 

  /**
   * @brief Convenient method to get the closest distance between the markers of a body part and the markers of an
   * other body part.
   * 
   * @param osimModel 
   * @param osimState 
   * @return std::map<std::string, double> 
   */
  std::map<std::string, double> getClosestMarkerFromOtherBody(osim::Model & osimModel, SimTK::State & osimState)
  {
    osimModel.realizeVelocity(osimState);
    std::map<std::string, Eigen::Vector3d> D_mark_pos = getMarkersPositions(osimModel,osimState);
    return( getClosestMarkerFromOtherBody(osimModel,D_mark_pos)  );
  }

  /**
   * @brief Get the closest distance between the markers of a body part and the markers of an
   * other body part.
   * 
   * @param osimModel 
   * @param D_pos 
   * @return std::map<std::string, double> 
   */
  std::map<std::string, double> getClosestMarkerFromOtherBody(osim::Model & osimModel, std::map<std::string, Eigen::Vector3d> D_pos)
  {

        osim::MarkerSet markerSet = osimModel.upd_MarkerSet();
        std::map<std::string,double>D_markers_closest;

        
        std::map<std::string, std::vector<int> > markers_by_frame;
        for (int i =0; i< markerSet.getSize(); i++)
        {   
            std::string frame_name = osimModel.upd_MarkerSet()[i].getParentFrameName();
            if (  markers_by_frame.find( frame_name) == markers_by_frame.end() )
            {
              markers_by_frame[frame_name] = std::vector<int>();
            }
            markers_by_frame[frame_name].push_back(i);
        }


        for ( std::map<std::string, std::vector<int> >::iterator it = markers_by_frame.begin(); it!= markers_by_frame.end(); it++)
        {   
            double dist_considered = 0.0;
            std::vector<int> all_index = it->second;
            // get min dist for each marker of body part
            for (int l = 0; l < all_index.size(); l++ )
            {
              
              double min_dist = 9999;
              int i = all_index[l];
              std::string name = osimModel.upd_MarkerSet()[i].getName();
              //SimTK::Vec3 pos =  osimModel.upd_MarkerSet()[i].getLocationInGround(osimState);

              //Eigen::Vector3d position; position <<  pos.get(0), pos.get(1), pos.get(2);

              Eigen::Vector3d position = D_pos[name];
              for (int j = 0; j< markerSet.getSize(); j++)
              {
                if (std::find(all_index.begin(),all_index.end(),j)==all_index.end())
                {
                  std::string name_j = osimModel.upd_MarkerSet()[j].getName();
                  Eigen::Vector3d position_j = D_pos[name_j];
                  //SimTK::Vec3 pos_j =  osimModel.upd_MarkerSet()[j].getLocationInGround(osimState);
                  //Eigen::Vector3d position_j; position_j <<  pos_j.get(0), pos_j.get(1), pos_j.get(2);
                  double dist = (position_j-position).norm();
                  if (dist < min_dist)
                  {
                    min_dist = dist;
                  }

                }
              }
                
              if (min_dist > dist_considered)
              {
                dist_considered = min_dist;
              }
        
            }
            // std::cout << "Final dist : " << dist_considered << std::endl;
            for (int l = 0; l < all_index.size(); l++ )
            {
              int i =  all_index[l];
              std::string name_c = osimModel.upd_MarkerSet()[i].getName();
              D_markers_closest[ name_c ] = dist_considered;
            }
        }
      return(D_markers_closest);
  } 

  /**
   * @brief Get the maximum and the minimum distance between all the markers of
   * the model.
   * 
   * @param osimModel 
   * @param osimState 
   * @param min_dist 
   * @param max_dist 
   */
  void getDistanceBetweenMarkers(osim::Model & osimModel, SimTK::State & osimState, double & min_dist, double & max_dist)
  {
    std::map<std::string, Eigen::Vector3d> D_markers_positions = getMarkersPositions(osimModel,osimState);
    double min = 9999;
    double max = 0.0;
  

    for (std::map<std::string, Eigen::Vector3d>::iterator it = D_markers_positions.begin(); it!=D_markers_positions.end(); it++)
    {
      Eigen::Vector3d mark1 = it->second;
      for (std::map<std::string, Eigen::Vector3d>::iterator it2 = it; it2!=D_markers_positions.end(); it2++)
      {
        Eigen::Vector3d mark2 = it2->second;

        double dist = (mark1-mark2).norm();
        if ( (dist < min ) && (dist > 0) )
        {
          min = dist;
        } 
        else if (dist > max)
        {
          max = dist;
        }
      }

   }
   min_dist = min;
   max_dist = max;

  }

  /**
   * @brief Print the positions of the markers of the model.
   * 
   * @param D_markers_positions 
   */
  void printMarkersPositions(std::map<std::string, Eigen::Vector3d> D_markers_positions)
  {
    printf(" ----  Markers Positions ---- \n");

    for (std::map<std::string, Eigen::Vector3d>::iterator it = D_markers_positions.begin(); it!=D_markers_positions.end(); it++)
    {
      Eigen::Vector3d mark = it->second;
      printf("Name %s : position : [%3.4f , %3.4f, %3.4f ] \n",it->first.c_str(),mark[0], mark[1],mark[2] );
    }
  printf(" -------- \n");
  }

  /**
   * @brief Convenient method to print the positions of the markers of the model (expressed into the global referential).
   * 
   * @param Model 
   * @param osimState 
   */
  void printMarkersPositions(osim::Model & Model, SimTK::State & osimState)
  {
    std::map<std::string, Eigen::Vector3d> D_markers_positions = getMarkersPositions(Model,osimState);
    printMarkersPositions(D_markers_positions);
  }

/**
 * @brief Executes the IK operation; given the markers positions, their weights, and eventually the coordinates
 * weights, obtains the coordinates values thanks to Opensim methods.
 * Please note that the coordinates values are not limited to their limits defined into the Opensim model.
 * 
 * 
 * NB : tracking seems to be only to follow particular trajectories, defined by function.
 * (see https://simtk.org/api_docs/opensim/api_docs/classOpenSim_1_1CoordinateReference.html#ac68eb7a53c5b1d7a28037f2a22dd6249)
 * For this reason, this is desactivate for now.
 * 
 * NB2 : seems possible to add it : see https://github.com/opensim-org/opensim-core/blob/master/OpenSim/Tools/InverseKinematicsTool.cpp,
 * line 4211 : possible to create CoordinateReference from Coordinate values.
 * Try it, associated with rtosim. Still not working. :c
 * The first lines are meant to deactivate tracking, as it is still not working for now. Comment them if you want to try.
 * 
 * NB3 : Understood issue. Tracking need assemble to be called first by the object InverseKinematicsSolver.
 * The issue is that it's impossible to modifiy MarkersReference once InverseKinematicsSolver has been created. Thus, a new InverseKinematicsSolver
 * must be called each time the position of markers has changed; and then, each time assemble must be called again. 
 * Two way to solve it : * recode the solver with an access at MarkersReference.
 *                      * do IK for multiple frames. All frames will be saved into MarkersReference. Then, fist one will have assemble,
 *                      then other will have track. Then, all should be published.
 * 

 * NB4 : lot of noise when using Coordinates References. Reason seems to be linked to the fact that values are directly taken from joint values from IK, and as there is no method
 * to do assemble again, the noise accumulates more and more without being corrected by an assemble.
 * Correction : after a certain number of frames, no CoordinatesReferences are taken into account : clean assemble.
 * 
 * @param osimModel 
 * @param s 
 * @param markers_positions 
 * @param markers_weights 
 * @param mode : 0 : assemble with coordinate; 1 : assemble from scratch
 * @return int 
 */
  int IKFromMarkersPositionsAndWeights(osim::Model & osimModel, SimTK::State & s, 
  std::map<std::string,Eigen::Vector3d> markers_positions, 
  std::map<std::string,double> markers_weights,
  std::map<std::string,double> coordinates_weights,
  int mode,
  double time)
  {
    //  if (mode ==0)
    // {
    //    mode = 1;
    //  }

    int result;
    osim::MarkersReference markers_ref{};

    osim::Set<osim::MarkerWeight> aweights = markers_ref.getMarkerWeightSet();

    for (std::map<std::string,double>::iterator it =markers_weights.begin(); it!= markers_weights.end(); it++)
    {
      aweights.cloneAndAppend(*(new osim::MarkerWeight(it->first, it->second)) );
    }

    std::vector<std::string> markerNames;
    osim::Array<std::string> L;
    aweights.getNames(L);

    for (int i = 0; i < L.getSize(); i++){
      markerNames.push_back(L.get(i));
    }
    int len = markerNames.size();
    SimTK::Matrix_<SimTK::Vec3> M{1, len}; 
    for (int i =0; i< markerNames.size(); i++)
    {
      Eigen::Vector3d pos = markers_positions[markerNames[i]];
      SimTK::Vec3 V = SimTK::Vec3();
    
      V.set(0,pos[0]);
      V.set(1,pos[1]);
      V.set(2,pos[2]);
    
      M.set(0,i,V);

    }
        
    osim::TimeSeriesTableVec3 table{ {time}  ,M,markerNames};
    //std::cout << "table : " << table << std::endl;

     for (int i = 0; i < aweights.getSize(); i++)
    {
     double weight = markers_weights[aweights[i].getName()];
     aweights[i].setWeight(weight); 
    } 


    osim::MarkersReference nmarker_ref{table,aweights};

    SimTK::Array_<OpenSim::CoordinateReference> coordinateRefs;
    if (mode == 0) // mode == 0
    {
      std::map<std::string,double> D_j_v = fosim::getJointsValues(osimModel,s);
      osim::CoordinateReference *coordRef = NULL;
      for (std::map<std::string,double>::iterator it=D_j_v.begin(); it!= D_j_v.end(); it++)
      {
        osim::Constant reference = osim::Constant(it->second);
        coordRef = new osim::CoordinateReference(it->first, reference);
        double weight = coordinates_weights[it->first];
        coordRef->setWeight(weight);
        coordinateRefs.push_back(*coordRef);
      }
      s.updTime() = time;

    }
    else
    {
      s.updTime() = time;
    }

    double constraintWeight = 0.001;

    OpenSim::InverseKinematicsSolver ikSolver(osimModel, nmarker_ref, coordinateRefs, constraintWeight);

    double solverAccuracy_ = 0.00001;

    ikSolver.setAccuracy(solverAccuracy_);



    try {
      if (mode > 0)
      {
      ikSolver.assemble(s);
      //std::cout << "assemble ok" << std::endl;
      }
      else if (mode == 0)
      {
      //s.updTime() = time;
      ikSolver.assemble(s);
      //ikSolver.track(s);
      //std::cout << "track ok" << std::endl;
      } 
      result = 1;

      
    }
    catch (...){
        if (mode > 0)
        {
          std::cerr << "Time " << s.getTime() << " Model not assembled" << std::endl;
        }
        else if (mode ==0)
        {
          std::cerr << "Time " << s.getTime() << " Model not tracked" << std::endl;
        } 
        result = 0;
    }





    // SimTK::Array_<double> markerErrors;
    // ikSolver.computeCurrentMarkerErrors(markerErrors);

    // std::cout << "markers errors Opensim : " << markerErrors << std::endl;

    //SimTK::Array_<double> markerErrors;
    //ikSolver.computeCurrentMarkerErrors(markerErrors);
/*     std::cout << "In fosim s : " << s.getQ() << std::endl;
    std::cout << "In fosim osimstate : " << osimState.getQ() << std::endl;
    for (int i = 0; i < osimModel.getNumCoordinates(); i++)
    {
    std::cout << "Coord s : " << coordinateRefs[i].getValue(s) << std::endl;
    std::cout << "Coord osimstate : " << coordinateRefs[i].getValue(osimState) << std::endl;
    } */
    return(result);
  }


/**
 * @brief (DEPRECIATED).  Executes the IK operation; given the markers positions, obtains the coordinates values thanks to Opensim methods. 
 * The weights of all the markers are set to 1.0.
 * Please note that the coordinates values are not limited to their limits defined into the Opensim model.
 * 
 * 
 * @param osimModel 
 * @param osimState 
 * @param markers_positions 
 * @param xml_taskset 
 */
  void IKFromMarkersPositions(osim::Model & osimModel, SimTK::State & osimState, std::vector<double> markers_positions, std::string xml_taskset)
  {

    SimTK::State s = osimState;//osimModel.initSystem();
    osim::MarkersReference markers_ref{};
    // = osim::MarkersReference();
        //s.setQ(s.getQ());
    osim::Set<osim::MarkerWeight> aweights = markers_ref.getMarkerWeightSet();
    osim::InverseKinematicsTool ikTool{xml_taskset};

    osim::IKTaskSet IKTaskSet = ikTool.getIKTaskSet();

    IKTaskSet.createMarkerWeightSet(aweights);

    //aweights.get(4).setWeight(0.0);
    
    // int i = 0;
    // double tst =  ( aweights.get(i) ).getWeight();

    // std::printf("size : %i \n" ,aweights.getSize());
    // std::printf("num : %f \n" , tst);

    std::vector<std::string> markerNames;
    osim::Array<std::string> L;
    aweights.getNames(L);

    for (int i = 0; i < L.getSize(); i++){
      markerNames.push_back(L.get(i));
    }
    int len = markerNames.size();
    SimTK::Matrix_<SimTK::Vec3> M{1, len}; 
    for (int i =0; i< markerNames.size(); i++)
    {
      
      SimTK::Vec3 V = SimTK::Vec3();
    
      V.set(0,markers_positions[3*i]);
      V.set(1,markers_positions[3*i+1]);
      V.set(2,markers_positions[3*i+2]);
    
      M.set(0,i,V);

    }
        
    osim::TimeSeriesTableVec3 table{ {0.0}  ,M,markerNames};
    //std::cout << "table : " << table << std::endl;


    osim::MarkersReference nmarker_ref{table,aweights};

    SimTK::Array_<OpenSim::CoordinateReference> coordinateRefs;

    double constraintWeight = 0.001;

    OpenSim::InverseKinematicsSolver ikSolver(osimModel, nmarker_ref, coordinateRefs, constraintWeight);

    double solverAccuracy_ = 0.00001;

    ikSolver.setAccuracy(solverAccuracy_);
    try {
      ikSolver.assemble(s);
      std::cout << "assemble ok" << std::endl;

      
    }
    catch (...){
      std::cerr << "Time " << s.getTime() << " Model not assembled" << std::endl;
    }

    SimTK::Array_<double> markerErrors;
    ikSolver.computeCurrentMarkerErrors(markerErrors);

    std::cout << markerErrors << std::endl;

  }

/**
 * @brief Read datas on a .csv tracking file made with Motive <= 1.10.2
 * 
 * 
*/
bool readCSVMotiveFile(std::string filename, std::map< double, std::map<std::string, Eigen::Vector3d>  > & CSVData, int & dataRate, int & numMarkers)
{

  bool success = false;
  std::ifstream csvFile;

  csvFile.open(filename.c_str());
  if (csvFile.is_open())
  {
    std::string line;
    std::vector<std::string> cols;

    // first line for caputre frame rate (= datarate)
    getline(csvFile, line);
    cols.clear();

    cols = splitString(line,",");

    bool keep = true;
    int k = 0;

    while (k < cols.size() && keep)
    {
      if (cols[k] == "Capture Frame Rate")
      {
        dataRate = std::stoi(cols[k+1]);
        keep = false;
      }
      else
      {
        k+=1;
      }
    }

    // next line blank
    getline(csvFile, line);
    // next line to get "Marker" . Get numMarkers

    std::map<int, std::vector<int> > markCols;

    getline(csvFile, line);
    cols.clear();

    cols = splitString(line,",");    

    k = 0;

    while (k < cols.size() )
    {
      if (cols[k] == "Marker")
      {
        std::vector<int> v = {k, k+1,k+2};
        markCols[k] = v;
        k+=3;
      }
      else
      {
        k+=1;
      }
    }

    numMarkers = markCols.size();

    // next line to transform from Unlabeled:5000 to Unlabeled 5000

    std::map<int, std::string> nameMark;

    getline(csvFile, line);
    cols.clear();

    cols = splitString(line,",");    

    for (std::map<int,std::vector<int> >::iterator it = markCols.begin(); it!=markCols.end(); it++)
    {
      std::string name = cols[it->first];
      std::vector<std::string> name_compo = splitString(name, ":");
      std::string corrected_name = "";
      for (int i = 0; i < name_compo.size()-1; i++)
      {
        corrected_name += name_compo[i] + " ";
      }
      corrected_name += name_compo[name_compo.size() -1];

      nameMark[it->first] = corrected_name;
    }

    // next 3 lines blank
    getline(csvFile, line);
    getline(csvFile, line);
    getline(csvFile, line);       

    // next lines data

    while (getline(csvFile,line))
    {
  
      cols.clear();

      cols = splitString(line,",");    
    
      // colomn 0 : num frame; column 1 : time ; next columns : (X,Y,Z) of each marker

      double time = std::stod(cols[1]);

      std::map<std::string,Eigen::Vector3d> frame;

      for (std::map<int,std::vector<int> >::iterator it = markCols.begin(); it!=markCols.end(); it++)
      {
        std::string name = nameMark[it->first];

        double X = std::stod(cols[it->second[0]]);
        double Y =  std::stod(cols[it->second[1]]);
        double Z =  std::stod(cols[it->second[2]]);

        Eigen::Vector3d pt(X,Y,Z);

        frame[name] = pt;

      }

      CSVData[time] = frame;

    }

    success =true;
  }


  return (success);




}



/**
 * @brief Writes the input positions of markers at different times into a .trc file. 
 * 
 * @param filename Absolute path to the .trc file that will be created.
 * @param TRCData Map object with the following structure : time : < Markers_name, Position_at_time>
 */
  void writeTRCFile(std::string filename, std::map< double, std::map<std::string, Eigen::Vector3d>  > TRCData, int dataRate)
  {

    std::vector<double> time_vec;
    std::vector<std::string> markerNames;

    int len_col = TRCData[0].size();
    int len_line = TRCData.size();

    SimTK::Matrix_<SimTK::Vec3> M{len_line, len_col}; 

    bool activate = true;
    int i =0;
    for (std::map< double, std::map<std::string, Eigen::Vector3d> >::iterator it = TRCData.begin(); it!= TRCData.end(); it++)
    {
      time_vec.push_back(it->first);
      std::map<std::string, Eigen::Vector3d> line = it->second;
      SimTK::Vec3 V = SimTK::Vec3();
      int j = 0;
      for (std::map<std::string, Eigen::Vector3d>::iterator it2 = line.begin(); it2!= line.end(); it2++)
      {
        if (std::find(markerNames.begin(), markerNames.end(), it2->first) == markerNames.end() )
        {
          markerNames.push_back(it2->first);
        }

        Eigen::Vector3d mark = it2->second;
        V.set(0, mark[0]);
        V.set(1, mark[1]);
        V.set(2, mark[2]);
        M.set(i,j,V);

        j++;
      }

      i++;

    }

    // complete table infos
    osim::TimeSeriesTableVec3 table{ time_vec, M, markerNames };

    table.addTableMetaData<std::string>("DataRate", std::to_string( dataRate) );
    table.addTableMetaData<std::string>("CameraRate", std::to_string(dataRate) ); 
    table.addTableMetaData<std::string>("NumFrames", std::to_string(len_line) ); 
    table.addTableMetaData<std::string>("NumMarkers", std::to_string(len_col) ); 
    table.addTableMetaData<std::string>("Units", "m"); 
    table.addTableMetaData<std::string>("OrigDataRate", std::to_string(dataRate) ); 
    table.addTableMetaData<std::string>("OrigDataStartFrame", "1"); 
    table.addTableMetaData<std::string>("OrigNumFrames", std::to_string(len_line) ); 
 
    osim::TRCFileAdapter TRCWriter;

    TRCWriter.write(table, filename);


  }

  /**
   * @brief Taken from rtosim package. Reads a .trc file, and only returns positions expressed at each frame (no NaN markers, or lines
   * without any data).
   * 
   * 
   * @param filename 
   * @param TRCData 
   * @param dataRate 
   */
  void readTRCFile(const std::string & filename, 
  std::map< double, std::map<std::string, Eigen::Vector3d>  > & TRCData,
  int & dataRate, 
  int & numMarkers,
  std::vector<int> & metrics)
  {
    int numOfFramesWithOneMissingMarker = 0;
    int numOfFramesWith3MissingMarker = 0;    
    int totalNumOfMissingMarkers = 0;
    int totalNumOfMarkers = 0;
    OpenSim::MarkerData trcFile(filename);
    
    dataRate = static_cast<unsigned>(trcFile.getDataRate());
    numMarkers = static_cast<unsigned>(trcFile.getNumMarkers());

    std::vector<std::string> markerNamesFromFile;
    toStdVector(trcFile.getMarkerNames(), markerNamesFromFile);

    for (size_t i(0); i < static_cast<size_t>(trcFile.getNumFrames()); ++i) {
      auto markersFromTrcArr(trcFile.getFrame(i).getMarkers());
      std::map<std::string,Eigen::Vector3d> markersFromTrc;
      int numMissMarkInFrame = 0;
      for (unsigned j(0); j < markersFromTrcArr.size(); ++j)
      {
          SimTK::Vec3 mark = markersFromTrcArr[j];
          Eigen::Vector3d eigenMark(mark.get(0), mark.get(1), mark.get(2));

          bool noneNan = !isnan(eigenMark[0]) && !isnan(eigenMark[1]) && !isnan(eigenMark[2]);
          bool notNull = eigenMark.norm() > 1e-16;
          if (noneNan && notNull)
          {
            markersFromTrc[markerNamesFromFile[j]]  = eigenMark;
          }
          else
          {
            numMissMarkInFrame +=1;
          }
          totalNumOfMarkers+=1;

      }
      if (numMissMarkInFrame >=1)
      {
        numOfFramesWithOneMissingMarker+=1;
      }
      if (numMissMarkInFrame >= 3)
      {
        numOfFramesWith3MissingMarker +=1;
      }

      totalNumOfMissingMarkers+= numMissMarkInFrame;

      double time=  trcFile.getFrame(i).getFrameTime();
      TRCData[time] = markersFromTrc;
    } 
    metrics.push_back(totalNumOfMarkers);
    metrics.push_back(totalNumOfMissingMarkers);    
    metrics.push_back(numOfFramesWithOneMissingMarker);
    metrics.push_back(numOfFramesWith3MissingMarker);



  }


  template<typename T, typename U>
  void toStdVector(const T& srcArray, U& dstVector) {
      dstVector.clear();
      int size = srcArray.getSize();
      dstVector.resize(size);
      for (int i = 0; i < size; ++i)
          dstVector.at(i) = srcArray.get(i);
  }



/**
 * @brief Writes the input values of the Coordinates of the Opensim model at different times into a .mot file.
 * Inspired from https://github.com/opensim-org/opensim-core/blob/master/OpenSim/Analyses/Kinematics.cpp
 * 
 * 
 * @param filename : name of the .mot file.
 * @param MOTData Map time:map_coordinate_value, with map_coordinate_value a map object whose elements are : 
 * name_of_the_coordinate:value_of_the_coordinate.
 */
  void writeMOTFile(std::string filename, std::map< double, std::map<std::string, double>  > MOTData)
  {
    
    osim::Storage MOTStore(1000,"Coordinates");
    char descrip[1024];

    strcpy(descrip, "\nUnits are S.I. units (second, meters, Newtons, ...)");
    strcat(descrip, "\nIf the header above contains a line with ");
    strcat(descrip, "'inDegrees', this indicates whether rotational values ");
    strcat(descrip, "are in degrees (yes) or radians (no).");
    strcat(descrip, "\n\n");

    MOTStore.setDescription(descrip);
    MOTStore.setInDegrees(false);

    osim::Array<std::string> labels;
    labels.append("time");
    
    std::map<std::string, double> first_line = MOTData[0];
    for (std::map<std::string, double>::iterator it2 = first_line.begin(); it2!= first_line.end(); it2++)
    {
      labels.append(it2->first);
    }

    MOTStore.setColumnLabels(labels);

    for (std::map< double, std::map<std::string, double> >::iterator it = MOTData.begin(); it!= MOTData.end(); it++)
    {
      double time = it->first;

      osim::Array<double> values;

      std::map<std::string, double> line = it->second;

      for (std::map<std::string, double>::iterator it2 = line.begin(); it2!= line.end(); it2++)
      {

        values.append(it2->second);

      }

      MOTStore.append(time, values );

    }


    MOTStore.print(filename);


  }

/**
 * @brief Convenient method to get a vector with all the bodies of the model into multiorder 
 * (= from the root of the model to its ends), and the map parent_body:all_direct_childrens_of_the_parent.
 * 
 * @param osimModel 
 * @param osimState 
 * @param body_in_multiorder 
 * @param D_parent_all_children 
 */
  void getMarkersBodiesInMultibodyOrder(osim::Model & osimModel, SimTK::State & osimState,
       std::vector<std::string> & body_in_multiorder,
     std::map<std::string, std::vector<std::string> > & D_parent_all_children)
{
  std::map<std::string,std::string> D_child_parent = getChildParentBody(osimModel);
  std::map<std::string,std::vector<std::string> > D_body_markers = getMarkersByBodyParts(osimModel,osimState);
  getMarkersBodiesInMultibodyOrder(D_child_parent, D_body_markers, body_in_multiorder, D_parent_all_children);

}

/**
 * @brief Returns a vector with all the bodies of the model into multiorder 
 * (= from the root of the model to its ends), and the map parent_body:all_direct_childrens_of_the_parent.
 * 
 * @param D_child_parent 
 * @param D_body_markers 
 * @param body_in_multiorder 
 * @param D_parent_all_children 
 */
  void getMarkersBodiesInMultibodyOrder(std::map<std::string,std::string> D_child_parent,
    std::map<std::string,std::vector<std::string> > D_body_markers,
     std::vector<std::string> & body_in_multiorder,
     std::map<std::string, std::vector<std::string> > & D_parent_all_children)
  {
  
  // just to be sure we start from scratch
  body_in_multiorder.clear();
  D_parent_all_children.clear();

  std::map<std::string, std::string> D_c_p_m = D_child_parent;

  // Search for the (unique) body part that is a direct child from ground.
  // This body part is the root of the model.
  auto itground= std::find_if(std::begin(D_c_p_m), std::end(D_c_p_m), [&](const std::pair<std::string,std::string> &pair)
  {
          return pair.second == "ground";
  });

  body_in_multiorder.push_back(itground->first);
  D_c_p_m.erase(itground);

  // next, let's navigate from root to final children. We will
  // then add the whole chain to body_in_multiorder.

  // first, find body parts directly children from global_root
  std::string global_root = body_in_multiorder[0];

  bool itnameC = true;
  std::vector<std::string> Dcons;

  while (itnameC)
  {
    // check if there is still an element that is a direct child of the global root body part
    auto itname = std::find_if(std::begin(D_c_p_m), std::end(D_c_p_m), [&](const std::pair<std::string,std::string> &pair)
    {
            return pair.second == global_root;
    });
    itnameC = (itname != D_c_p_m.end());

    std::vector<std::string> current_link;
    std::string child; 

    if (itnameC)
    { 
      // if this is the case, let's navigate from this element to an extremity of the model.
      child = itname->first;
      current_link.push_back(child);
      D_c_p_m.erase(itname);
      // add this element to the list of children of the body root.
      Dcons = D_parent_all_children[itground->first];
      if (std::find(Dcons.begin(), Dcons.end(), child  )== Dcons.end() )
      {
        D_parent_all_children[itground->first].push_back(child);
      }

    }

    bool itchildC = itnameC;

    while (itchildC)
    {
      // check if the child is an extremity of the model or not.
      // (let's call this child child_i)
      // A body part is defined as an extremity of the model if this body part do not have
      // any child, i.e this body part is the parent of no body part.
      auto itchild = std::find_if(std::begin(D_c_p_m), std::end(D_c_p_m), [&](const std::pair<std::string,std::string> &pair)
      {
              return pair.second == child;
      });

      itchildC = (itchild !=D_c_p_m.end() );

      if (itchildC)
      {
        // in case a child has been found :
        // (lets call the child of child_i by the name of child_i+1)

        // add only bodies that have markers
        if (D_body_markers.find(itchild->first)!= D_body_markers.end() )
        { 
          // add child_i+1 to the list of children of the body root.
          Dcons = D_parent_all_children[itground->first];
          if (std::find(Dcons.begin(), Dcons.end(), itchild->first  )== Dcons.end()   )
          {
            D_parent_all_children[itground->first].push_back(itchild->first);
          }

          // add child_i+1 to the list of children of all the elements of the 
          // current explored chain..
          for (int i =0; i < current_link.size(); i++)
          {
            Dcons = D_parent_all_children[current_link[i]];
            if (std::find(Dcons.begin(), Dcons.end(), itchild->first  )== Dcons.end()   )
            {
              D_parent_all_children[current_link[i]].push_back(itchild->first);
            }
          }
          // add the child to the current explored chain.
          current_link.push_back(itchild->first);
        }        
        // do the same procedure with child_i+1
        child = itchild->first;
      }

    }
    // once an extremity has been found, add the elements of the explored chain
    // to the list body in multiorder
    for (int k = 0; k < current_link.size(); k++)
    {
      // check if the element was not already in body_in_multiorder (could happen with a bug in the model?)
      if (std::find(body_in_multiorder.begin(), body_in_multiorder.end(), current_link[k]) == body_in_multiorder.end())
      {
        // current_link selected is really a body with markers
        
        if (D_body_markers.find(current_link[k])!= D_body_markers.end() )
        {
          body_in_multiorder.push_back(current_link[k]);
        }
      } 
    }

  }


  }

/**
 * @brief Convenient method to attribute weights to markers according to the distances of their respective bodies
 * to the ends and the root link of the model.
 * 
 * @param osimModel 
 * @param osimState 
 * @return std::map<std::string,double> 
 */
  std::map<std::string,double> computeMarkersWeights(osim::Model & osimModel, SimTK::State & osimState)
  {
    std::map<std::string,std::string> D_child_parent = getChildParentBody(osimModel);
    std::map<std::string,std::vector<std::string> > D_body_markers = getMarkersByBodyParts(osimModel,osimState);
    std::vector< std::string > markers_bodies_multiorder;
    std::map<std::string, std::vector< std::string > > D_parent_all_children;
    getMarkersBodiesInMultibodyOrder(D_child_parent, D_body_markers,markers_bodies_multiorder, D_parent_all_children);
    std::map<std::string,Eigen::Vector3d> D_mark_pos_local =  getMarkersPositions(osimModel,osimState,"Local");
    std::map<std::string,double> D_markers_weights = computeMarkersWeights(markers_bodies_multiorder, D_child_parent, D_mark_pos_local, D_body_markers);

    return(D_markers_weights);

  }

/**
 * @brief Attribute weights to markers according to the distances of their respective bodies
 * to the ends and the root link of the model.
 * 
 * Ex : for two_arms_only_marks, should be : 
 * torso : 5
 * humerus_l : 1
 * radius_l : 2
 * humerus_r : 1
 * radius_r : 2
 * 
 * @param markers_bodies_multiorder 
 * @param D_child_parent 
 * @param D_mark_pos_local 
 * @param D_body_markers 
 * @return std::map<std::string,double> 
 * 
 * 
 */
    std::map<std::string,double> computeMarkersWeights(std::vector<std::string> markers_bodies_multiorder,
    std::map<std::string,std::string> D_child_parent,
  std::map<std::string,Eigen::Vector3d> D_mark_pos_local,
  std::map<std::string,std::vector<std::string> > D_body_markers)
  {

    std::map<std::string,double> D_markers_weights;

    std::string root = markers_bodies_multiorder[0];

    std::vector<std::string> root_marks = D_body_markers[root];

    // a maximum weight is given to the markers of the root link.
    for (int j = 0; j < root_marks.size(); j++)
    {
      D_markers_weights[root_marks[j] ] = markers_bodies_multiorder.size();
    }

    int origin_body_weight = 1;
    int body_weight = origin_body_weight;

    for (int i = 1; i < markers_bodies_multiorder.size(); i++)
    {
      std::string body_name = markers_bodies_multiorder[i];

      std::string parent_name = D_child_parent[body_name];

      if (parent_name == root)
      {
        body_weight = origin_body_weight;
      }

      std::vector<std::string> body_marks = D_body_markers[body_name];

      for (int j = 0; j < body_marks.size(); j++)
      {
        D_markers_weights[body_marks[j] ] = body_weight;
      }

      body_weight++;


    }


    

    return(D_markers_weights);
  }
/**
 * @brief Print the values of the weights of the markers of the model.
 * 
 * @param markersWeights 
 */
  void printMarkersWeights(std::map<std::string,double> markersWeights)
  {
    for (std::map<std::string,double>::iterator it = markersWeights.begin(); it!= markersWeights.end(); it++)
    {
      std::cout << "Name : " << it->first << " ; weight : " << it->second << std::endl;
    }
  }
  

  Eigen::Matrix4f getBodyTransformInGround(osim::Model & osimModel, SimTK::State & osimState, std::string body_name)
  {
    osim::BodySet bodies = osimModel.upd_BodySet();
    SimTK::Transform ground2body;
    Eigen::Matrix4f g2b_transform; g2b_transform.setZero();
    bool found = false;
    int i =0;
    
    while (i < bodies.getSize())
    {
      //std::cout << bodies[i].getName() << std::endl;
      if (bodies[i].getName() == body_name)
      {
        ground2body = osimModel.upd_BodySet()[i].getTransformInGround(osimState);
        i = bodies.getSize();
        found = true;
      }
      i++;
    }

    if (found)
    {

        g2b_transform.setIdentity();

        SimTK::Vec4 q = ground2body.R().convertRotationToQuaternion().asVec4();
        Eigen::Quaternion<float> qe( q.get(0),q.get(1),q.get(2),q.get(3) );
        Eigen::Matrix3f R = qe.toRotationMatrix();

        SimTK::Vec3 p = ground2body.p();
        Eigen::Vector3f pf(p.get(0), p.get(1), p.get(2));

        g2b_transform.block<3,3>(0,0) = R;
        g2b_transform.block<3,1>(0,3) = pf;
    }


    return(g2b_transform);
  }




}
